mod self_encryptor;
mod simple_storage;
mod simple_storage_error;
mod storage;

pub use self_encryptor::{SelfEncryptor, SelfEncryptorError};
pub use simple_storage::SimpleStorage;
pub use simple_storage_error::SimpleStorageError;
pub use storage::Storage;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let mut simple_storage = SimpleStorage::new();
        let mut self_encryptor = SelfEncryptor::new(&mut simple_storage);
        self_encryptor.write(vec![1, 2, 3], vec![4, 5, 6]).unwrap();
        println!("\n\n{}", self_encryptor.read(&vec![1, 2]).unwrap_err());
        println!("{:?}\n\n", self_encryptor.read(&vec![1, 2]).unwrap_err());
        assert_eq!(self_encryptor.read(&vec![1, 2, 3]).unwrap(), vec![4, 5, 6]);

        for i in 0..255 {
            match self_encryptor.write(vec![i], vec![i]) {
                Err(SelfEncryptorError::Storage(ref error @ SimpleStorageError::LowBalance)) => {
                    println!("{} - continuing...", error);
                }
                Err(error) => {
                    println!("{} - aborting\n\n", error);
                    break;
                }
                _ => (),
            }
        }
    }
}
