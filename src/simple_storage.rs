use std::collections::HashMap;

use simple_storage_error::SimpleStorageError;
use storage::Storage;

pub struct SimpleStorage {
    entries: HashMap<Vec<u8>, Vec<u8>>,
}

impl SimpleStorage {
    pub fn new() -> SimpleStorage {
        SimpleStorage { entries: HashMap::new() }
    }
}

impl Storage<SimpleStorageError> for SimpleStorage {
    fn get(&self, name: &[u8]) -> Result<Vec<u8>, SimpleStorageError> {
        match self.entries.get(name) {
            Some(ref value) => Ok((*value).clone()),
            None => Err(SimpleStorageError::Get),
        }
    }

    fn put(&mut self, name: Vec<u8>, data: Vec<u8>) -> Result<(), SimpleStorageError> {
        if self.entries.len() == 10 {
            return Err(SimpleStorageError::Put(vec![]));
        }
        match self.entries.insert(name, data) {
            Some(ref ejected_value) => Err(SimpleStorageError::Put(ejected_value.clone())),
            None => {
                if self.entries.len() > 5 {
                    Err(SimpleStorageError::LowBalance)
                } else {
                    Ok(())
                }
            }
        }
    }
}
