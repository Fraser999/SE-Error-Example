use std::error::Error;
use std::fmt::{self, Display, Formatter};
use std::marker::PhantomData;

use storage::{Storage, StorageError};

#[derive(Debug)]
pub enum SelfEncryptorError<E: StorageError> {
    Serialisation,
    Storage(E),
}

impl<E: StorageError> Display for SelfEncryptorError<E> {
    fn fmt(&self, formatter: &mut Formatter) -> fmt::Result {
        match *self {
            SelfEncryptorError::Serialisation => write!(formatter, "Serialisation/parsing error"),
            SelfEncryptorError::Storage(ref error) => write!(formatter, "Storage error: {}", error),
        }
    }
}

impl<E: StorageError> ::std::error::Error for SelfEncryptorError<E> {
    fn description(&self) -> &str {
        match *self {
            SelfEncryptorError::Serialisation => "Serialisation error",
            SelfEncryptorError::Storage(ref error) => error.description(),
        }
    }
}

impl<E: StorageError> From<E> for SelfEncryptorError<E> {
    fn from(error: E) -> SelfEncryptorError<E> {
        SelfEncryptorError::Storage(error)
    }
}




pub struct SelfEncryptor<'a, E: StorageError, S: 'a + Storage<E>> {
    storage: &'a mut S,
    phantom: PhantomData<E>,
}

impl<'a, E: StorageError, S: Storage<E>> SelfEncryptor<'a, E, S> {
    pub fn new(storage: &'a mut S) -> SelfEncryptor<'a, E, S> {
        SelfEncryptor {
            storage: storage,
            phantom: PhantomData,
        }
    }

    pub fn write(&mut self, name: Vec<u8>, value: Vec<u8>) -> Result<(), SelfEncryptorError<E>> {
        Ok(try!(self.storage.put(name, value)))
    }

    pub fn read(&mut self, name: &[u8]) -> Result<Vec<u8>, SelfEncryptorError<E>> {
        Ok(try!(self.storage.get(name)))
    }
}
