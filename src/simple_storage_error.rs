use std::error::Error;
use std::fmt::{self, Display, Formatter};

use storage::StorageError;

#[derive(Debug, Clone)]
pub enum SimpleStorageError {
    Get,
    Put(Vec<u8>),
    LowBalance,
}

impl Display for SimpleStorageError {
    fn fmt(&self, formatter: &mut Formatter) -> fmt::Result {
        match *self {
            SimpleStorageError::Get => write!(formatter, "Error while getting data"),
            SimpleStorageError::Put(_) => write!(formatter, "Error while putting data"),
            SimpleStorageError::LowBalance => write!(formatter, "Low balance"),
        }
    }
}

impl Error for SimpleStorageError {
    fn description(&self) -> &str {
        match *self {
            SimpleStorageError::Get => "Get error",
            SimpleStorageError::Put(_) => "Put error",
            SimpleStorageError::LowBalance => "Low balance",
        }
    }
}

impl StorageError for SimpleStorageError {}
