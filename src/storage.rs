use std::error::Error;

pub trait StorageError: Error {}

pub trait Storage<E: StorageError> {
    /// Fetch the data bearing the name
    fn get(&self, name: &[u8]) -> Result<Vec<u8>, E>;
    /// Insert the data bearing the name.
    fn put(&mut self, name: Vec<u8>, data: Vec<u8>) -> Result<(), E>;
}
